import configparser
import redis
import time
from flask import Flask
from os import environ

# Load config from file.
config = configparser.ConfigParser()
config.read('./config.ini')

if environ.get('REDIS_HOST'):
    redis_host = environ.get('REDIS_HOST')
else:
    redis_host = config['redis']['host']

if environ.get('REDIS_PORT'):
    redis_port = environ.get('REDIS_PORT')
else:
    redis_port = config['redis']['port']

app = Flask(__name__)
cache = redis.Redis(
    host=redis_host,
    port=redis_port
    )


def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
