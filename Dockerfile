FROM python:3.8.0-buster
COPY app /opt/app
WORKDIR /opt/app
EXPOSE 5000
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
